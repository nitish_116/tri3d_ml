# README #

### What is this repository for? ###

* All the ml applications for tri3d will be hosted on this repo
* guessing_springs is the first app , which will deal with code to create and train images specific to this problem

### Installation Steps ###
* git clone git@bitbucket.org:nitish_116/tri3d_ml.git
* mkvirtualenv tri3d_assignment
* cd tri3d_ml
* python setup.py
* cp /usr/local/lib/python2.7/site-packages/cv\* /Users/nitish/.virtualenvs/tri3d_assignment/lib/python2.7/site-packages/ (modify this according to ur cv* installation and virtual environment)
* cd web/
* ./manage.py shell 
* from guessing_springs import utils
* utils.generate_images_main('garment_internal_example',image_scale=25,coverage_pct=0.8)

Now you should see images FALSE_<ID>.jpg and TRUE_<ID>.jpg in outputs/garment_internal_example/ folder


### Trouble Shooting ###
* We strongly recommend you to use virtualenv , setup.py calls pip install -r requirements.txt , to avoid clashes use virtualenv
* Install opencv and then copy the packages to your virtual environment

### Reading Material ###
https://drive.google.com/drive/u/0/folders/0B8_QTRTseBcFNkRmWkloTlFSRW8

### Who do I talk to? ###

* Nitish Reddy Parvatham, nitish@tri3d.in