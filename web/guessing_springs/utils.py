from django.conf import settings
from models import GarmentInternal,GarmentImage,PatternImage
import imp
from pprint import pprint
import os
from itertools import product
def inspect_module(module_object,cutoff=10000):
    import inspect
    attributes = inspect.getmembers(module_object, lambda a:not(inspect.isroutine(a)))
    out = [a for a in attributes if not(a[0].startswith('__') and a[0].endswith('__'))]
    for cur_pair in out:
        if len(str(cur_pair[1])) < cutoff:
            print(cur_pair)
            print(" ")
        else:
            print(cur_pair[0],type(cur_pair[1]),len(str(cur_pair[1])))
            print(" ")


def generate_multiple_garments(folder_list,output_keyword='test',image_scale=5,coverage_pct=1,generate_images=False,apply_random_rotation=False,iterations=1):
	for cur_folder in folder_list:
		for loop in range(iterations):
			generate_images_main(cur_folder,output_keyword=output_keyword+str(loop),image_scale=image_scale,coverage_pct=coverage_pct,generate_images=generate_images,apply_random_rotation=apply_random_rotation)
	pass


"""
folder_name , the target garments that needs to be present in the inputs/ folder which will read
image_scale , by default the image_size would be 144 X 144 , image_scale factor would be applied to scale the images 
for an input of image_scale == 20 , output image_size would be 2880 X 2880 pixels 
coverage_pct -- is the bounding circle diameter / image_size square , beyond which the pattern vertices won't cross
if the image_size = 1000 X 1000 and coverage_pct = 0.5 , the pattern vertices will lie only in the circle of radius 250 pixels with centre at the centre of the image

prints the cnt of all , good and bad images generated in this round
"""
def generate_images_main(folder_name,output_keyword='test',image_scale=25,coverage_pct=0.8,generate_images=True,apply_random_rotation=False):

	GI = GarmentInternal(folder_name=folder_name,output_keyword=output_keyword)
	
	GI.fetch_springs_data()##fetches all the Edges where Springs have to be added
	GI.fetch_all_edges()
	if generate_images:
		os.system(GI.output_folder_path)
	print("print guruurr")
	cnt = 0
	cnt_good = 0
	cnt_bad = 0


   ##Now I Have all the edges of all the patterns and I also have True Edges Where Springs Are to be Added This all I got it from the configuration files 
   ##Now Next is to genrate the Image Data set With the edges marked both the true and false cases 
   ##enumerate just adds the counter to the variable
   ##  
	for cnt1,elem1 in enumerate(GI.edge_list_all):
		#print(elem1)##(patternname,edgeno,edgeno)
		for cnt2,elem2 in enumerate(GI.edge_list_all):
			if elem1 != elem2 and elem1[0] != elem2[0]:##not for the same pattern and not within the same pattern you can add springs
				if 1:#cnt == 0:
					p_first = elem1[0].replace('pattern','')
					#print("printing",p_first)
					p_second = elem2[0].replace('pattern','')
					p_list = [p_first,p_second]
					#print(p_list)
					if p_first == sorted(p_list)[0]:
						cur_spring = (elem1,elem2)##two edges form a spring they will not belong to same pattern
						cur_spring_rev = (elem2,elem1)##spring reverse
						good = False
						if cur_spring in GI.springs_dict.values() or cur_spring_rev in GI.springs_dict.values():
							id = 'TRUE__' + str(cnt) 
							cnt_good+= 1
							good = True
						else:
							id = 'FALSE__' + str(cnt)
							cnt_bad+=1
						
						if generate_images:
							if 1:#cnt < 1:
								print("Generating Image -- ",id,cur_spring)
								garment_image = GarmentImage(id=id,garment_internal=GI,image_scale=image_scale,coverage_pct=coverage_pct,spring=cur_spring,apply_random_rotation=apply_random_rotation)
								garment_image.initialize_pattern_images()
								garment_image.draw_images()
								garment_image.stitch_images()
						cnt+=1	
    
	print(folder_name," | cnt -- ",cnt,"cnt_good -- ",cnt_good,"#pattern -- ",len(GI.pattern_layers),'#edges -- ',len(GI.edge_list_all))
	print("guur")
	return 'success'
