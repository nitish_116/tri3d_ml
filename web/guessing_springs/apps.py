from __future__ import unicode_literals

from django.apps import AppConfig


class GuessingSpringsConfig(AppConfig):
    name = 'guessing_springs'
