from __future__ import unicode_literals

from django.db import models
from django.conf import settings
import os
import imp
from pprint import pprint
import numpy as np
import cv2
from PIL import Image
import math
# Create your models here.

"""
Will have list of pattern images classes
This MegaImage will be a composition of all the PatternImage's
Each Pattern Image will be given the same space

Each Garment Image is for a spring
spring is defined by two edges 
edge is defined by a pattern and two points 

(pattern1,0,1)
(pattern2,0,1)

"""
class GarmentImage():
	def __init__(self,id,garment_internal,image_scale,coverage_pct,spring=(),apply_random_rotation=False):
		self.PIXELS_LEN = 144 # will be working with #patterns <= 16
		self.pixel_unit = 5

		self.id = id
		self.coverage_pct = coverage_pct
		self.garment_internal = garment_internal #Linked to the GarmentInternal , where all the folder structure is managed
		self.image_scale = image_scale #will be using a multiple of PIXELS_LEN for the image_size of the garment 

		self.image_size = self.image_scale*self.PIXELS_LEN
		self.apply_random_rotation = apply_random_rotation
		self.spring = spring
		self.startup()
		self.folder_path = self.garment_internal.output_folder_path
		if not os.path.isdir(self.folder_path):
			os.system("mkdir " + self.folder_path)
		self.img_path =  self.folder_path + str(self.id) + '.jpg'

	def set_pattern_count(self):
		#no: of active patterns of the garment 
		self.pattern_count = len(self.garment_internal.pattern_layers)
		return self.pattern_count

	def set_block_size(self):
		#will be dividing the garment in block_size number of blocks
		if self.pattern_count == 1:
			self.block_size = 1
		elif self.pattern_count > 1 and self.pattern_count <= 4:
			self.block_size = 4
		elif self.pattern_count > 4 and self.pattern_count <= 9:
			self.block_size = 9
		elif self.pattern_count > 9 and self.pattern_count <= 16:
			self.block_size = 16
		else:
			self.block_size = -1
			raise ValueError("Cannot deal pattern count beyond 16 for now")


	"""
	say we have 7 patterns we will be using 9 blocks and placing each of the 7 patterns in one of these 9 blocks
	"""
	def set_pattern_image_size(self):
		if self.image_size % self.block_size == 0:
			self.pattern_image_size = self.image_size/self.block_size
		else:
			raise ValueError("Interested in working only with integer block sizes")

	"""
	could do this initiailization itself
	"""
	def startup(self):
		self.set_pattern_count()
		self.set_block_size()
		self.set_pattern_image_size()

		self.spring_patterns = {}
		if len(self.spring) == 2:
			for edge in list(self.spring):
				if edge[0] in self.garment_internal.pattern_layers:
					self.spring_patterns[edge[0]] = (edge[1],edge[2])
			if len(self.spring_patterns) != 2:
				self.spring_patterns = {}
			

	"""
	Will be creating the pattern image instances
	"""
	def initialize_pattern_images(self):
		self.pattern_images_list = {}
		for cur_pattern in self.garment_internal.pattern_layers:
			edge_list = ()
			if cur_pattern in self.spring_patterns:
				edge_list = self.spring_patterns[cur_pattern]
				#print("printing the edge_list",edge_list)
			cur_config_file = self.garment_internal.get_pattern_config_file(cur_pattern)
			self.pattern_images_list[cur_pattern] = PatternImage(parent_id=self.id,folder_path=self.folder_path,name=cur_pattern,config_file=cur_config_file,image_size=self.pattern_image_size,pixel_unit=self.pixel_unit,coverage_pct=self.coverage_pct,edge_list=edge_list,apply_random_rotation=self.apply_random_rotation)

		self.get_maximum_pixel_unit()

	def get_maximum_pixel_unit(self):

		max_pixel_unit_list = []
		for cur_pattern in self.pattern_images_list:
			P = self.pattern_images_list[cur_pattern]
			max_pixel_unit_list.append(P.get_maximum_pixel_unit())

		self.pixel_unit = min(max_pixel_unit_list)

		for cur_pattern in self.pattern_images_list:
			P = self.pattern_images_list[cur_pattern]
			P.set_pixel_unit(self.pixel_unit)
			P.set_pixel_coords()



	
	def draw_images(self):
		for cur_pattern in self.pattern_images_list:
			P = self.pattern_images_list[cur_pattern]
			P.draw_image()
#this is to paste different patterns into a one single final image
	def stitch_images(self):
		p_size = self.pattern_image_size
		g_size = self.image_size
		blank_image = Image.new("RGB", (g_size, g_size), "black")#this line is to create new Image with a black background
		sq_size = int(pow(self.block_size,0.5))
		cnt = 0
		for cur_pattern in self.pattern_images_list:
			P = self.pattern_images_list[cur_pattern]
			I = Image.open(P.img_path)
			I = I.resize((g_size/sq_size,g_size/sq_size),Image.ANTIALIAS)	
			x = cnt/sq_size
			y = cnt%sq_size
			blank_image.paste(I,(g_size*x/sq_size,g_size*y/sq_size))
			os.system("rm -rf "+P.img_path)
			cnt+=1

		blank_image.save(self.img_path)


		pass


"""
Will be linked to a patternconfig file, from where we will be getting the initial coords, curve definitions
Also we will be providing the list of edges that needs to be coloured
edge is defined using two adjacent points (0,1) , (6,7) & (n-1,0)

size - (x,x) no:of x and y pixels . We are looking only at square images(GROUND RULE)
GROUND RULE : centre of polygon will be at the centre of the image 
rotation - r , that needs to applied 
scale - s , this defines the 

"""
class PatternImage():
	def __init__(self,folder_path,parent_id,name,config_file,image_size,pixel_unit,coverage_pct,edge_list=(),apply_random_rotation=False):
		self.name = name
		self.config_file = config_file
		self.image_size = image_size
		self.pixel_unit = pixel_unit #defines number of pixels for 1unit distance in the coords provided
		self.coverage_pct = coverage_pct
		self.edge_list = edge_list
		self.folder_path = folder_path
		self.parent_id = parent_id
		self.rotation_val = 0
		self.apply_random_rotation = apply_random_rotation

		self.rotation = 0 #could be a number from 0 --> 360
		self.translation = (0,0) #pass the translation interms of pixels in x&y directions
		self.point_pixels = {}

		self.img_path = self.folder_path + str(self.parent_id) + '__' + str(self.name) + '.jpg' #needs to be passed from GarmentImage
		self.load_coords()
		self.set_pixel_coords()


	def set_rotation(self,r):
		self.rotation = r
	def set_translation(self,t):
		self.translation = t

	def apply_rotation(self,relative_coords,angle):
		import math
		import random
		def rotate(origin, point, angle):
		    """
		    Rotate a point counterclockwise by a given angle around a given origin.

		    The angle should be given in radians.
		    """
		    ox, oy = origin
		    px, py = point

		    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
		    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
		    return qx, qy

		input_t = (relative_coords[0],relative_coords[1])
		origin = (0,0)
		

		ans = rotate(origin,input_t,angle)

		return np.array((ans[0],ans[1],0))

	def apply_translation(self):
		pass

	"""
		load the coonfig_initia_dicts_pattern.py and get the coords usinf the generate_initial_coords function
		calculate the centre coords and their pixel values as well according to the image_size
	"""
	def load_coords(self):
		pconfig = imp.load_source('module.name',self.config_file)
		self.coords = pconfig.generate_initial_coords({})
		self.centre = np.mean(np.array(self.coords.values()),axis=0)
		self.centre_pixels = (int(self.image_size/2),int(self.image_size/2))
		
	def get_maximum_pixel_unit(self):

		rel_list = []
		for point_index in self.coords:
			original_coords = np.array(self.coords[point_index])
			relative_coords = np.subtract(original_coords,self.centre)
			rel_list.append(relative_coords)

		max_dist = np.max(np.absolute(np.array(rel_list)))
		
		return int(self.coverage_pct*self.image_size/(2*max_dist))

	def set_pixel_unit(self,pixel_unit):
		self.pixel_unit = pixel_unit
		

	def set_pixel_coords(self):
		import random
		angle = random.randint(1,360)*math.pi/180
		for point_index in self.coords:
			original_coords = np.array(self.coords[point_index])
			relative_coords = np.subtract(original_coords,self.centre)
			if self.apply_random_rotation:
				
				relative_coords = self.apply_rotation(relative_coords,angle)

			final_x = self.centre_pixels[0] + relative_coords[0]*self.pixel_unit
			final_y = self.centre_pixels[1] + relative_coords[1]*self.pixel_unit
			self.point_pixels[point_index] = [final_x,final_y]


	def get_point_list(self):
		point_list = []
		for cnt in range(len(self.point_pixels)):
			point_list.append(self.point_pixels[cnt])
		return np.int32([np.array(point_list)])
		# return [np.array(point_list).reshape((-1,1,2))]
	
	def get_point_list_of_edge(self,edge_list):
		point_list = []
		for cnt in range(len(self.point_pixels)):
			if cnt in list(edge_list):
				point_list.append(self.point_pixels[cnt])
		return np.int32([np.array(point_list)])


	def draw_image(self):
		img = np.zeros((self.image_size,self.image_size,3), np.uint8)
		pts = self.get_point_list()
		cv2.polylines(img,pts,True,(255,255,255))##this code prints all the pts in white color
		if self.edge_list:
		   cv2.polylines(img,self.get_point_list_of_edge(self.edge_list),True,(0,0,255))##this code has to color the edges in red color
		cv2.imwrite(self.img_path,img)
		pass

"""
Need superficial information about folder structure , pointing to the correct config files and understanding if certain patterns/springs are active or not 
Let's not get too deep about the pattern points and curves here for now , let's deal that separately
"""
class GarmentInternal():

	def __init__(self,folder_name,output_keyword):
		self.folder_name = folder_name
		self.output_keyword = output_keyword
		self.is_correct = False
		self.error_message = "No Error Message for now :P"
		self.startup()

	def flush_attributes(self):
		gfolder = settings.GARMENTS_FOLDER
		self.base_dir = gfolder + self.folder_name + '/'
		if not os.path.isdir(self.base_dir):
			self.is_correct = False
			self.error_message = "BASE DIR " + self.base_dir +" does not exist"
			return self.is_correct

		self.garments_dir = self.base_dir + 'garment/bodycon/' #hardcoded
		self.garment_config_file = self.garments_dir + 'config_initial_dicts.py'
		if not os.path.exists(self.garment_config_file):
			self.is_correct = False
			self.error_message = "GARMENT CONFIG " +  self.garment_config_file + "does not exist"
			return self.is_correct

		self.patterns_dir = self.base_dir + 'pattern/'
		if not os.path.isdir(self.patterns_dir):
			self.is_correct = False
			self.error_message = "PATTERN DIR " + self.patterns_dir + "does not exist"
			return self.is_correct

		self.simulations_dir = self.base_dir + 'simulation/'
		self.simulation_config_file = self.simulations_dir + 'settings_simulation_original.py'
		if not os.path.exists(self.simulation_config_file):
			self.is_correct = False
			self.error_message = "SIMULATION DIR " + self.simulation_config_file + "does not exist"
			return self.is_correct

		self.is_correct = True

		self.output_folder_path = settings.OUTPUTS_FOLDER + self.folder_name + '__' + self.output_keyword + '/'
		return self.is_correct
##map_patterns just gets the name of all the patterns and store it in the pattern_name_folder_mapping
#creating dictionary for 
#  patter1----folder name
#pattern2 -----folder name
# ,,,,,,,,,,,,,,,,,,,,,,
	def map_patterns(self): 
		from utils import inspect_module
		sim_settings = imp.load_source('module.name', self.simulation_config_file)
		
		pattern_settings = sim_settings.simulation_settings_file['garment_settings'][0]['pattern_settings']
		self.pattern_name_folder_mapping = {}
		for cur_pattern in pattern_settings:
			self.pattern_name_folder_mapping[cur_pattern] = pattern_settings[cur_pattern]['settings_folder'].split('/')[-1]
####?????????????
	def fetch_actives(self):
		garment_config = imp.load_source('module.name',self.garment_config_file)
		self.pattern_layers = garment_config.generate_preferential_stitching()
		self.spring_layers = garment_config.generate_preferential_stitching_springs()

	def startup(self):

		self.flush_attributes()
		 #inspect_module(self)
		f = open(self.garments_dir+'utils_old.py','w')
		import sys
		if self.garments_dir not in sys.path:
			sys.path.append(self.garments_dir)
		f.write('import os')
		f.close()
		
		self.map_patterns()
		self.fetch_actives()

	#provides the config_initial_dicts_pattern.py file , this needs to be loaded to get all the coords, curves etc 
	def get_pattern_config_file(self,pattern_name):
		if pattern_name in self.pattern_name_folder_mapping:
			ans = self.patterns_dir + self.pattern_name_folder_mapping[pattern_name] + '/config_initial_dicts_pattern.py' 
			if os.path.exists(ans):
				return ans
		return ""

	def fetch_edge_list(self,pattern_name):
		pattern_file = self.get_pattern_config_file(pattern_name)
		pconfig = imp.load_source('module.name',pattern_file)
		coords = pconfig.generate_initial_coords({})
		points_count = len(coords)

		edge_list = []
		cnt = 0
		for cur_point in coords:
			if cnt < points_count - 1:
				edge_list.append((pattern_name,cnt,cnt+1))
			else:
				edge_list.append((pattern_name,0,cnt))
			cnt+=1
		return edge_list

	def fetch_all_edges(self):
		self.edge_list_all = []
		for cur_pattern in self.pattern_layers:
			self.edge_list_all.extend(self.fetch_edge_list(cur_pattern))



	def fetch_springs_data(self):
		def create_tuple(vg):
			l = vg.split('_')
			return (l[0],int(l[2]),int(l[3]))

		garment_config = imp.load_source('module.name',self.garment_config_file)
		springs_dict = garment_config.generate_springs_dict()
		self.springs_dict = {}
		for cur_spring in springs_dict:
			vg1 = springs_dict[cur_spring]['vertex_group1']
			vg2 = springs_dict[cur_spring]['vertex_group2']
			if create_tuple(vg1)[0] in self.pattern_layers and create_tuple(vg2)[0] in self.pattern_layers:
				self.springs_dict[cur_spring] = (create_tuple(vg1),create_tuple(vg2))

		return self.springs_dict
