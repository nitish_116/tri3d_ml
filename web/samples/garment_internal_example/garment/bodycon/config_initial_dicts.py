



def find_where_am_i():
    import os,sys,inspect
    CUR_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    cur_dir_split = CUR_DIR.split("/") 
    BASE_DIR  = "/".join(cur_dir_split[0:-1]) + "/"
    UTILS_DIR = BASE_DIR + 'utils/'
    if UTILS_DIR not in sys.path:
        sys.path.append(UTILS_DIR)

    where_am_i = 'internal'
    if 'tri3d_core' in BASE_DIR:
        where_am_i = 'core'
    elif 'tri3d_internal' in BASE_DIR:
        where_am_i = 'internal'
    else:
        raise ValueError("Not in core or internal")

    return where_am_i

P_l = 0

if find_where_am_i() == 'internal':
    P_l = 0

def generate_preferential_stitching():

    preferential_stitching = {
        
        # #main body
        'pattern1' : P_l,
        'pattern2' : P_l,
        # 'pattern3' : P_l,
        # 'pattern4' : P_l,
        # 'pattern5' : P_l,
        # 'pattern6' : P_l,




    }
    return preferential_stitching

def generate_preferential_stitching_springs():
    preferential_stitching = {
        
        #sleeve body connections
        'spring1' : P_l,
        'spring2' : P_l,  
        'spring3' : P_l,
        'spring4' : P_l,
        'spring5' : P_l,
        'spring6' : P_l,
        'spring7' : P_l,
        'spring8' : P_l,
        'spring9' : P_l,
        'spring10' : P_l,
        'spring11' : P_l,
        'spring12' : P_l,


    }

    return preferential_stitching

def generate_springs_dict():
    springs_dict = {
        #SPRINGS_START

        'spring1':{
            'type' : 'edge_edge',
            'vertex_group1' : 'pattern2_edge_9_10',
            'vertex_group2' : 'pattern1_edge_9_10',
            'reverse' : -1,
            'skip' : -1, #negative skip means no skip
            'add' : True,
        },
            

            

        'spring2':{
            'type' : 'edge_edge',
            'vertex_group1' : 'pattern2_edge_10_11',
            'vertex_group2' : 'pattern1_edge_10_11',
            'reverse' : -1,
            'skip' : -1, #negative skip means no skip
            'add' : True,
        },
            

        'spring3':{
            'type' : 'edge_edge',
            'vertex_group1' : 'pattern2_edge_0_11',
            'vertex_group2' : 'pattern1_edge_0_11',
            'reverse' : -1,
            'skip' : -1, #negative skip means no skip
            'add' : True,
        },
            

        'spring4':{
            'type' : 'edge_edge',
            'vertex_group1' : 'pattern2_edge_1_2',
            'vertex_group2' : 'pattern1_edge_1_2',
            'reverse' : -1,
            'skip' : -1, #negative skip means no skip
            'add' : True,
        },
            

        'spring5':{
            'type' : 'edge_edge',
            'vertex_group1' : 'pattern2_edge_2_3',
            'vertex_group2' : 'pattern1_edge_2_3',
            'reverse' : -1,
            'skip' : -1, #negative skip means no skip
            'add' : True,
        },
            

        'spring6':{
            'type' : 'edge_edge',
            'vertex_group1' : 'pattern2_edge_3_4',
            'vertex_group2' : 'pattern1_edge_3_4',
            'reverse' : -1,
            'skip' : -1, #negative skip means no skip
            'add' : True,
        },
            

        'spring7':{
            'type' : 'edge_edge',
            'vertex_group1' : 'pattern3_edge_0_1',
            'vertex_group2' : 'pattern1_edge_7_8',
            'reverse' : 1,
            'skip' : -1, #negative skip means no skip
            'add' : True,
        },
            

        'spring8':{
            'type' : 'edge_edge',
            'vertex_group1' : 'pattern4_edge_0_1',
            'vertex_group2' : 'pattern1_edge_5_6',
            'reverse' : 1,
            'skip' : -1, #negative skip means no skip
            'add' : True,
        },
            

        'spring9':{
            'type' : 'edge_edge',
            'vertex_group1' : 'pattern2_edge_5_6',
            'vertex_group2' : 'pattern5_edge_0_1',
            'reverse' : 1,
            'skip' : -1, #negative skip means no skip
            'add' : True,
        },
            

        'spring10':{
            'type' : 'edge_edge',
            'vertex_group1' : 'pattern6_edge_0_1',
            'vertex_group2' : 'pattern2_edge_7_8',
            'reverse' : 1,
            'skip' : -1, #negative skip means no skip
            'add' : True,
        },
            

        'spring11':{
            'type' : 'edge_edge',
            'vertex_group1' : 'pattern6_edge_2_3',
            'vertex_group2' : 'pattern3_edge_2_3',
            'reverse' : -1,
            'skip' : -1, #negative skip means no skip
            'add' : True,
        },
            

        'spring12':{
            'type' : 'edge_edge',
            'vertex_group1' : 'pattern5_edge_2_3',
            'vertex_group2' : 'pattern4_edge_2_3',
            'reverse' : -1,
            'skip' : -1, #negative skip means no skip
            'add' : True,
        },
            
		#SPRINGS_END

    }
    return springs_dict




def calc_offsets(offsets_original,avatar_name,p,posner_dict,patterns_dict,original_placement_path='',sim_num=0):
    where_am_i = find_where_am_i()

    if 1:#where_am_i == 'internal':
        from zodbpickle import pickle
        import copy
        import os
        offsets_dict = copy.deepcopy(offsets_original)
        if os.path.exists(original_placement_path):
            offsets_dict = pickle.load(open(original_placement_path,'rb'))
        return offsets_dict
