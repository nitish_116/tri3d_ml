import math

def generate_initial_coords(measurements_dict):
    # from pprint import pprint 
    ans = {}
    #POINTS_START
    ans[0] = (4.466578006744385, -0.03823452442884445, 0.0)
    ans[1] = (5.150375843048096, -0.03156508877873421, 1.5113698070834403e-09)
    ans[2] = (5.150375843048096, 2.961352825164795, 1.5113698070834403e-09)
    ans[3] = (4.470849514007568, 2.9649224281311035, 7.556849035417201e-10)
    #POINTS_END

    return ans

def generate_face_vertex_map(settings_main):
    face_vertex_map = {
        0:[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],
        # 0:[0,1,2,5],
        # 1:[5,2,3,4],
    }
    return face_vertex_map

def return_additional_edge_enhancers(settings_main):
    default_points = settings_main['default_cuts']
    # default_points = 10
    curve_cuts = settings_main['curve_cuts']
    enhanced_edges = {
        
        # (0,(2,3)) :{
        #     'curve': 'parabola',
        #     'points': curve_cuts,
        #     'reverse': 1,
        #     'b_point' : (1,0),
        # },  
        
        # (0,(7,8)) : {
        #     'curve' : 'parabola',
        #     'points' : curve_cuts,
        #     'reverse': 1,
        #     'b_point' : (0,1), 
        # },
        
        
    }
    

    return enhanced_edges

def curve_internal_points(vertex,point, num_of_points = 3):
    #assuming y = a(x-h)^2 + k as the equation of parabola with (h,k) vertex and a given point
    h = vertex[0]
    k = vertex[1]
    x1 = point[0]
    y1 = point[1]
    a = float(y1-k)/(pow((x1-h),2))
    list_x = []
    points = []
    for ii in range (0,num_of_points):
        x=x1+(h-x1)*(ii+1)*0.25
        y=a*(pow((x-h),2))+k
        list_x.append(x)
        points.append((x,y,0))

    return points
