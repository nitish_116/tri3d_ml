import math

def generate_initial_coords(measurements_dict):
    # from pprint import pprint 
    ans = {}
    #POINTS_START
    ans[0] = (-13.38122844696045, -22.567853927612305, 2.842170943040401e-14)
    ans[1] = (13.307644844055176, -22.53944969177246, 2.842170943040401e-14)
    ans[2] = (10.71415901184082, -8.116081237792969, 2.842170943040401e-14)
    ans[3] = (9.034626007080078, -0.6649818420410156, 2.842170943040401e-14)
    ans[4] = (8.508014678955078, 5.940528869628906, 2.842170943040401e-14)
    ans[5] = (6.5557684898376465, 10.681995391845703, 2.842170943040401e-14)
    ans[6] = (5.896069526672363, 10.68087387084961, 2.842170943040401e-14)
    ans[7] = (-6.170170783996582, 10.696063995361328, 2.842170943040401e-14)
    ans[8] = (-6.833637237548828, 10.668525695800781, 2.842170943040401e-14)
    ans[9] = (-8.24422550201416, 5.95050048828125, 2.842170943040401e-14)
    ans[10] = (-8.999861717224121, -0.6018295288085938, 2.842170943040401e-14)
    ans[11] = (-10.387155532836914, -7.873600006103516, 2.842170943040401e-14)
    #POINTS_END

    return ans

def generate_face_vertex_map(settings_main):
    face_vertex_map = {
        0:[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],
        # 0:[0,1,2,5],
        # 1:[5,2,3,4],
    }
    return face_vertex_map

def return_additional_edge_enhancers(settings_main):
    default_points = settings_main['default_cuts']
    # default_points = 10
    curve_cuts = settings_main['curve_cuts']
    enhanced_edges = {
        (0,(4,5)) :{
            'curve': 'parabola',
            'points': curve_cuts,
            'reverse': 1,
            'b_point' : (1,0),
        },  
        (0,(8,9)) :{
            'curve': 'parabola',
            'points': curve_cuts,
            'reverse': 1,
            'b_point' : (0,1),
        },     
    }
    return enhanced_edges

def curve_internal_points(vertex,point, num_of_points = 3):
    #assuming y = a(x-h)^2 + k as the equation of parabola with (h,k) vertex and a given point
    h = vertex[0]
    k = vertex[1]
    x1 = point[0]
    y1 = point[1]
    a = float(y1-k)/(pow((x1-h),2))
    list_x = []
    points = []
    for ii in range (0,num_of_points):
        x=x1+(h-x1)*(ii+1)*0.25
        y=a*(pow((x-h),2))+k
        list_x.append(x)
        points.append((x,y,0))

    return points
