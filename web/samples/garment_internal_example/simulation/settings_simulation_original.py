#META_LEVEL ARGUMENTS
default_cuts = 0
make_single_face = True

folder_p = "pre_prod_fad5_4_blue_crepe/"

generator_version = "v3"
curve_cuts = 8

simulation_settings_file = {
        'settings_folder': folder_p+'basic',
        '2d-3d': '3d_three',
        'avatar_settings':{
            0:{
                'settings_folder': folder_p +'basic',
                'human_file_path' : '/Users/nitish/Desktop/IO/tri3d/MAIN/tri3d_internal/wgaps/core/v0/settings_config/pre_prod_bodycon/avatar/basic/current.mhx2',
            },
        },
        'garment_settings':{
            0:{
                'settings_folder': folder_p + 'bodycon',
                'measurements_dict' : {
                                        'base': 18,
                                        'bust': 19.5,
                                        'neck_width':7,
                                        'hip':21,
                                        'waist':16,
                                        'armhole_length':8.5,
                                        'shoulder_length':3,
                                        'neck_depth':2,
                                        'garment_length':30,
                                        'hip_height':10,
                                        'bust_height':20,
                                        'waist_height':15,
                                        'waist_inner':23,
                                        'bust_inner':19,
                                        'base_inner':24.5,
                                        'garment_inner_length':15,  
                                    },                      
                'minor_y_off' : 3,
                'posner_dict' : {
                    'y_off' : 3,
                    'sl_y_offset' : 1.5,
                    'y_off_outer' : 5,

                    'z_off' : 2,
                    'sl_z_offset' : 1,
                    'z_off_outer' : -7,

                    'x_off': 5,
                    'sl_x_offset' : 0,  
                    'sl_rot_angle' : 0.15, 
                    'x_off_outer' : 5,  
                },
                'bake_image' : True,
                'default_cuts' : 0,
                'need_springs' : True,
                'add_uv_texture' : True,
                'tex_image' : 'tex_image.jpg',
                'disp_image': 'disp_image.jpg',
                'button_image' : 'button_image.jpg',
                'active_pattern' : 'pattern1',
                'default_material': 'Cotton',
                'images_flder' : '/Users/nitish/Desktop/IO/tri3d/MAIN/tri3d_internal/wgaps/core/v0/settings_config/prod/garment/basic/images_folder/',
                'objs_foler' : '/Users/nitish/Desktop/IO/tri3d/MAIN/tri3d_internal/wgaps/core/v0/settings_config/prod/garment/basic/objs_folder/',
                'pattern_generation' : 'draw',
                'port_type' : 'blend',
                'active_pattern' : 'pattern3',
                'stiffness_patterns' : ['collar1','collar2','collar3','collar4','collar5','collar6','collar7','collar8','cuff1','cuff2','cuff3','cuff4', 'pattern8','pattern9'],
                'edge_group_assignment' : 'v2',
                'basic_unit' : 1,
                'max_volume' : 0.5,
                'pattern_settings':{
                # # Faduice _demo dress 1        
                    'pattern1' : {
                        'settings_folder' : folder_p+'body_front',
                        'default_cuts' : default_cuts,
                        'make_single_face':make_single_face,
                        'generator_version' : generator_version,
                        'image_file':'pattern1.jpeg',
                        'reverse':False,
                        'curve_cuts':curve_cuts,  
                    },

                    'pattern2' : {
                        'settings_folder' : folder_p+'body_back',
                        'default_cuts' : default_cuts,
                        'make_single_face':make_single_face,
                        'generator_version' : generator_version,
                        'image_file':'pattern1.jpeg',
                        'reverse':True,
                        'curve_cuts':curve_cuts,  
                    },

                    'pattern3' : {
                        'settings_folder' : folder_p+'strap_front',
                        'default_cuts' : default_cuts,
                        'make_single_face':make_single_face,
                        'generator_version' : generator_version,
                        'image_file':'pattern1.jpeg',
                        'reverse':False,
                        'curve_cuts':curve_cuts,  
                    },  
                    'pattern4' : {
                        'settings_folder' : folder_p+'strap_front',
                        'default_cuts' : default_cuts,
                        'make_single_face':make_single_face,
                        'generator_version' : generator_version,
                        'image_file':'pattern1.jpeg',
                        'reverse':False,
                        'curve_cuts':curve_cuts,  
                    },
                    'pattern5' : {
                        'settings_folder' : folder_p+'strap_back',
                        'default_cuts' : default_cuts,
                        'make_single_face':make_single_face,
                        'generator_version' : generator_version,
                        'image_file':'pattern1.jpeg',
                        'reverse':True,
                        'curve_cuts':curve_cuts,  
                    },  
                    'pattern6' : {
                        'settings_folder' : folder_p+'strap_back',
                        'default_cuts' : default_cuts,
                        'make_single_face':make_single_face,
                        'generator_version' : generator_version,
                        'image_file':'pattern1.jpeg',
                        'reverse':True,
                        'curve_cuts':curve_cuts,  
                    },  
                },
            },
        },

        'bake_settings' : {
            'use_sewing_springs' : True,
            'sewing_force_max' : 40,
            'use_self_collision' : True,
            'mass' : 0.05,
            'structural_stiffness' : 40,
            'bending_stiffness' : 0.5,
            'quality' : 5,
            'spring_damping' : 20,
            'distance_min' : 0.2,
            'distance_repel' : 0.21,
            'self_distance_min' : 0.6,
            'self_collision_quality' : 2,

            'frame_num': 20,
            'frame_num_list':{
                '0':20,
                '1':60,
                '2':30,
            },
            'frame_num2': 20,
            'keyframe_scaling_value' : (1,1,1),

            'bake_type': 'gravity_apply',


            'stiffness':True,
            'stiffness_group':'stiffness_group',
            'structural_stiffness_scale':500,
            'bend_stiffness_scale': 500,
            'smooth' : True,
            'subsurface': False,

            'frame_num_scale' : 30,
            'frame_num_all' :60,
            'keyframe_scaling' : True,
            'gravity' : 1,
            'velocity' : 0.98,
            'apply_modifier' : False,
            'apply_smooth' : False,
            'edge_collapse' : False,
        },

        'override_settings':{
            '0':{
                'source' : 'load_draped_garment',
                'layer': 0,
                'frame_num_all':1,
                'frame_num_scale':1, 
            },
        }
           
    }
