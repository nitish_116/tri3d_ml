import os
import string
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_DIR = string.replace(BASE_DIR,"\\","/") + "/"

#LOCAL_KEY
LOCAL_KEY = '' #Either enter your name here or create the local_key.txt in one folder above
local_key_file = BASE_DIR + '../local_key.txt'
if os.path.isfile(local_key_file):
    f = open(local_key_file,'rb')
    LOCAL_KEY = f.read().strip()



#MAIN FOLDER_NAMES
INPUTS_FOLDER = BASE_DIR + 'inputs/'
OUTPUTS_FOLDER = BASE_DIR + 'outputs/'
SAMPLES_FOLDER = BASE_DIR + 'samples/'


GARMENTS_FOLDER = INPUTS_FOLDER + 'garments_tri3d_internal_files/'



